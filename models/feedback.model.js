var mongoose = require('mongoose');
var Schema= mongoose.Schema;

var feedbackSchema  = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	time:Date,
	content: String
});

var Feedback = mongoose.model('Feedback', feedbackSchema, 'feedback');

module.exports = Feedback;