var mongoose = require('mongoose');
var Schema= mongoose.Schema;

var userSchema  = new Schema({
	email: String,
	password: String,
	name: String,
	avatar: String,
	phone: String,
	address: String,
	cmnd: String
});

var User = mongoose.model('User', userSchema, 'users');

module.exports = User;