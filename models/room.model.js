var mongoose = require('mongoose');
var Schema= mongoose.Schema;

var roomSchema  = new Schema({
	number: String,
	type: String,
	price: String,
	description: String,
	img: String,
	ratingRoom:Number
});

var Room = mongoose.model('Room', roomSchema, 'rooms');

module.exports = Room;