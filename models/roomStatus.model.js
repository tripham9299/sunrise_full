var mongoose = require('mongoose');
var Schema= mongoose.Schema;

var roomStatusSchema  = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	roomId:{
		type: Schema.Types.ObjectId,
		ref: 'Room'
	},
	status:String,
	night:String,
	checkin:Date,
	checkout:Date,
	adults:Number,
	children:Number
});

var RoomStatus = mongoose.model('RoomStatus', roomStatusSchema, 'roomStatus');

module.exports = RoomStatus;