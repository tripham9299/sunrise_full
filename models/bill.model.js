var mongoose = require('mongoose');
var Schema= mongoose.Schema;

var billSchema  = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	roomNumber:Number,
	time: Date,
	payment:String,
	usedTime:String,
	status:String,
	adults:Number,
	children:Number
});

var Bill = mongoose.model('Bill', billSchema, 'bills');

module.exports = Bill;