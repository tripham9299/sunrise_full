var mongoose = require('mongoose');
var Schema= mongoose.Schema;

var commentSchema  = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	roomId:{
		type: Schema.Types.ObjectId,
		ref: 'Room'
	},
	content: String,
	rating:Number,
	time:Date
});

var Comment = mongoose.model('Comment', commentSchema, 'comments');

module.exports = Comment;