var User = require('../models/user.model');
const Bill = require('../models/bill.model');
module.exports.index = async function(req, res) {
  var bills = await Bill.find().populate('userId');
  console.log(bills);
	res.json(bills);
};