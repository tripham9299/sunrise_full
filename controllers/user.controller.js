var User = require('../models/user.model');
const Bill = require('../models/bill.model');
const RoomStatus= require('../models/roomStatus.model');

module.exports.index = async function(req, res) {
  var users = await User.find();
	res.json(users);
};
module.exports.profile= async function(req,res){
  var profile= await User.findOne({_id:req.signedCookies.userId});
  var currentRoom= await RoomStatus.find({userId:req.signedCookies.userId}).populate('roomId');
  var userbill= await Bill.find({userId:req.signedCookies.userId});
  var data=[profile,userbill,currentRoom];
  res.json(data);
}
module.exports.postCreate = async function(req, res) {
  req.body.avatar = req.file.path.split('\\').slice(1).join('/');
  try {
	    var newUsers= await User.create({name:req.body.name, email:req.body.email, password:req.body.password, phone: req.body.phone, address:req.body.address,cmnd:req.body.cmnd, avatar:req.body.avatar});
	    res.json("ok");
    } catch (error) {
    	console.log(error);
    	res.json({message : error})
    }
};