const Room = require('../models/room.model');
const RoomStatus= require('../models/roomStatus.model');
const Bill = require('../models/bill.model');

module.exports.index = async function(req, res) {
  var rooms = await Room.find();
	res.json(rooms);
};
module.exports.roomDetail = async function(req, res) {
	let id = req.params.id;
  var d = new Date();
  var n= d.getTime();
  var room = await Room.findOne({ _id : id});
  var room_status= await RoomStatus.findOne({roomId:id});
  var check_in= Date.parse(room_status.checkin);
  var check_out= Date.parse(room_status.checkout);
  if(check_in<=n&&n<=check_out){
    var setStatus1= await RoomStatus.updateOne({roomId:id},{status:'1'});
  }
  if(check_out<n){
    var setStatus0= await RoomStatus.updateOne({roomId:id},{status:'0',userId:null,checkin:null,checkout:null,night:'0',adults:0,children:0});
  }
	res.json(room);
};
module.exports.roomCreate = async function(req, res) {
  	try {
	    var newRoom= await Room.create({number:req.body.number, type:req.body.type, price:req.body.price, description: req.body.description, img:"/img/room3.jpg",ratingRoom:5});
	    var newStatus = await RoomStatus.create({userId:null,roomId:newRoom._id, checkin:null,checkout:null,status:"0",adults:0,children:0,roomNumber:req.body.number});
	    res.json("ok");
    } catch (error) {
    	console.log(error);
    	res.json({message : error});
    }
};
module.exports.roomBooking = async function(req, res) {
    var id= req.params.id;
    var d= new Date();
    try {
      var checkStatus = await RoomStatus.findOne({roomId:id});
      if(checkStatus.status!=0){
        res.json("Sorry! This room has been booked or is in use. Please select another room.")
      }
      else{
        var newbooking = await RoomStatus.updateOne({roomId:id},{$set:{userId:req.signedCookies.userId, checkin:req.body.checkin, checkout:req.body.checkout, adults: req.body.adults,children:req.body.children,night:req.body.night,status:"2"}});
        var newbill = await Bill.create({userId:req.signedCookies.userId,roomNumber:req.body.roomNumber,time:d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate(),payment:req.body.payment,usedTime:req.body.checkin+" to "+req.body.checkout,status:"0",adults:req.body.adults, children:req.body.children});
        res.json("ok");
      }
    } catch (error) {
      console.log(error);
      res.json({message : error});
    }
};
module.exports.edit= async function(req,res){
  var id= req.body.idRoom;
  console.log(req.body.number);
  var edit= await Room.updateOne({_id:id},{number:req.body.number,type:req.body.type, price:req.body.price, description:req.body.description,img:'/img/room4.jpg'});
  console.log(edit);
  res.json("ok");
}
module.exports.roomstatus= async function(req,res){
  var roomstatus= await RoomStatus.find().populate('userId').populate('roomId');
  // console.log(roomstatus);
  res.json(roomstatus);
}

