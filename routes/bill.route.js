var express = require('express');

var controller = require('../controllers/bill.controller');

var router = express.Router();

// router.get('/login', controller.login);

router.get('/', controller.index);

module.exports = router;
