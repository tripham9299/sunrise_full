var User = require('../models/user.model');

module.exports.requireAuth = function(req, res, next) {
  if (!req.signedCookies.userId) {
    res.json("false");
    return;
  }

  var user = User.findOne({
    id: req.signedCookies.userId
  });

  if (!user) {
    res.json("false");
    return;
  }

  // res.locals.user = user;

  next();
};
