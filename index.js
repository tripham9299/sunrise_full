var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var userRoute = require('./routes/user.route');
var authRoute = require('./routes/auth.route');
// var authMiddleware = require('./middlewares/auth.middleware');
var roomRoute = require('./routes/room.route');
var billRoute = require('./routes/bill.route');

var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost:27017/sunrise';
mongoose.connect(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true});
//Ép Mongoose sử dụng thư viện promise toàn cục
mongoose.Promise = global.Promise;
//Lấy kết nối mặc định
var db = mongoose.connection;
//Ràng buộc kết nối với sự kiện lỗi (để lấy ra thông báo khi có lỗi)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var port = 5000;

app.set('view engine','pug');
app.set('views','./views');
app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.use(cookieParser('adhahdcjh13255'));
app.use(express.static('public'));

app.get('/', function(req,res){
	res.send("welcome");
});

app.use('/users', userRoute);
app.use('/rooms',roomRoute);
app.use('/auth',authRoute);
app.use('/bills',billRoute);

app.listen(port, function(){
	console.log('Server listening on port '+ port);
});